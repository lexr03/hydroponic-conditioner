#include "stepper.hpp"

Stepper_28BYJ::Stepper_28BYJ(uint8_t pins[4], uint8_t enable){
    for(int i = 0; i < 4; ++i){
        motor_pins[i] = pins[i];
        pinMode(motor_pins[i], OUTPUT);
    }

    enable_pin = enable;
    pinMode(enable_pin, OUTPUT);
    digitalWrite(enable_pin, 0);
}

void Stepper_28BYJ::init(uint8_t pins[4], uint8_t enable){
    for(int i = 0; i < 4; ++i){
        motor_pins[i] = pins[i];
        pinMode(motor_pins[i], OUTPUT);
    }

    enable_pin = enable;
    pinMode(enable_pin, OUTPUT);
    digitalWrite(enable_pin, 0);
}

void Stepper_28BYJ::set_target(int steps){
    steps_remaining = steps;
}

int Stepper_28BYJ::get_steps_remaining(){
    return steps_remaining;
}

void Stepper_28BYJ::handle_stepper(){
    if(steps_remaining){
        step();
    }
    else{
        disable();
    }
}

void Stepper_28BYJ::enable(bool set_slave){
    is_slave = set_slave;
    digitalWrite(enable_pin, 1);
}

void Stepper_28BYJ::disable(){
    digitalWrite(enable_pin, 0);
}

void Stepper_28BYJ::step(){
    if(!is_slave){
        for(int i = 0; i < 4; ++i){
            digitalWrite(motor_pins[i], step_lut[state] & 1 << i);
        }
        ++state;
        if(state == 8){
            state = 0;
        }
    }
    --steps_remaining;
    ++steps_total;
}