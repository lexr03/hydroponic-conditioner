#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WiFiMulti.h> 
#include <ArduinoOTA.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>   // Include the WebServer library
#include <FS.h>
#include <ArduinoJson.h>

#include "stepper.hpp"
#include "oled.hpp"

// TODO: create configuration hotspot when no connection is available 
// TODO: display pump status on screen

#define NUM_PUMPS 3
#define WIFI_TIMEOUT_S 5
#define STEPS_PER_ML 16068

ESP8266WiFiMulti wifiMulti;     // Create an instance of the ESP8266WiFiMulti class, called 'wifiMulti'

ESP8266WebServer server(80);

uint8_t pins[4] = {D3, D4, D5, D6};
uint8_t enable_pins[3] = {D0, D8, D7};

Stepper_28BYJ pumps[NUM_PUMPS];

Display display;

String getContentType(String filename); // convert the file extension to the MIME type
bool handleFileRead(String path);       // send the right file to the client (if it exists)

StaticJsonDocument<200> doc;
std::unique_ptr<char[]> buf;

StaticJsonDocument<200> payload;

const char* WIFI_SSID = NULL;
const char* WIFI_PASS = NULL; 

const char* OTA_NAME = NULL;
const char* OTA_PASS = NULL;

const char* MDNS_NAME = NULL;


////////////////////////////
//JSON Config file loading:
bool loadConfig(){
  File configFile = SPIFFS.open("/config.json", "r");
  if(!configFile){
    Serial.println("Failed to open config file");
    return false;
  }

  size_t size = configFile.size();
  if(size > 1024){
    Serial.println("Config file too large");
    return false;
  }

  buf = std::unique_ptr<char[]>(new char[size]);

  configFile.readBytes(buf.get(), size);

  // StaticJsonDocument<200> doc;
  auto error = deserializeJson(doc, buf.get());
  if (error) {
    Serial.println("Failed to parse config file");
    return false;
  }

  WIFI_SSID = doc["WIFI_SSID"];
  WIFI_PASS = doc["WIFI_PASS"];

  OTA_NAME = doc["OTA_NAME"];
  OTA_PASS = doc["OTA_PASS"];

  MDNS_NAME = doc["MDNS_NAME"];

  Serial.println("Config Data:");
  Serial.println(WIFI_SSID);
  Serial.println(WIFI_PASS);
  Serial.println(OTA_NAME);
  Serial.println(OTA_PASS);
  Serial.println(MDNS_NAME);
  
  return true;
}
////////////////////////////

void handleRoot() {
  server.send(200, "text/html", "<form action=\"/pump\" method=\"POST\">\
              <b>Hydroponic Conditioner Web Interface</b></br>\
              Enter a pump number (0-2) followed by the number of ml requested of that pump. Pumping will start immediately after \"Run\" is pressed.</br></br>\
              <b>To halt pumping:</b> Request 0 ml to stop the pump.</br><hr></br>\
              <b>Pump (0-2):\t</b></br>\
              <input type=\"text\" name=\"pump_number\" placeholder=\"0\"></br></br>\
              <b>Requested ml:\t</b></br>\
              <input type=\"text\" name=\"req_ml\" placeholder=\"0\"></br></br>\
              <input type=\"submit\" value=\"Run\"></form>");
}

int handleRemote(){
  Serial.println("Received Payload:");
  Serial.println(server.arg("plain"));
  server.sendHeader("Location","/"); 
  auto error = deserializeJson(payload, server.arg("plain"));
  if (error) {
    Serial.println("Failed to parse payload");
    return false;
  }

  for (int i = 0; i < NUM_PUMPS; ++i){
    // if(payload.containsKey(String(i))){
    //   int steps = payload[String(i)]["steps"];
    //   pumps[i].set_target(steps);
    // }
    if(payload.containsKey(String(i))){
      int ml = payload[String(i)]["ml"];
      pumps[i].set_target(ml * STEPS_PER_ML);
    }
  }
  return true;
}

void handlePumpRequest(){
  int pump = server.arg("pump_number").toInt();
  int ml = server.arg("req_ml").toInt();

  Serial.print("pump: ");
  Serial.println(pump);
  
  Serial.print("ml: ");
  Serial.println(ml);


  pumps[pump].set_target(ml * STEPS_PER_ML);

  server.sendHeader("Location","/");        // Add a header to respond with a new location for the browser to go to the home page again
  server.send(303); 
}

String getContentType(String filename) { // convert the file extension to the MIME type
  if (filename.endsWith(".html")) return "text/html";
  else if (filename.endsWith(".css")) return "text/css";
  else if (filename.endsWith(".js")) return "application/javascript";
  else if (filename.endsWith(".ico")) return "image/x-icon";
  return "text/plain";
}

bool handleFileRead(String path) { // send the right file to the client (if it exists)
  Serial.println("handleFileRead: " + path);
  if (path.endsWith("/")) path += "index.html";         // If a folder is requested, send the index file
  String contentType = getContentType(path);            // Get the MIME type
  if (SPIFFS.exists(path)) {                            // If the file exists
    File file = SPIFFS.open(path, "r");                 // Open it
    size_t sent = server.streamFile(file, contentType); // And send it to the client
    file.close();                                       // Then close the file again
    return true;
  }
  Serial.println("\tFile Not Found");
  return false;                                         // If the file doesn't exist, return false
}

void generate_header_string(char* header_string){
  sprintf(header_string, "IP:%s", WiFi.localIP().toString().c_str());
}


// Wifi functions:

bool attempt_connection(){
  if(wifiMulti.run() == WL_CONNECTED){
    Serial.println('\n');
    Serial.print("Connected to ");
    Serial.println(WiFi.SSID());              // Tell us what network we're connected to
    Serial.print("IP address:\t");
    Serial.println(WiFi.localIP()); 
    return true;
  }
  return false;
}

int init_wifi(){
  wifiMulti.addAP(WIFI_SSID, WIFI_PASS);   // add Wi-Fi networks you want to connect to

  Serial.println("Connecting ...");
  ulong winit_t0 = millis();
  while (!attempt_connection()) { // Wait for the Wi-Fi to connect: scan for Wi-Fi networks, and connect to the strongest of the networks above
    delay(250);
    Serial.print('.');
    if((millis() - winit_t0)/1000 > WIFI_TIMEOUT_S){
      Serial.println("Connection timed out.");
      return -1;
    }
  }
  return 0;
}

int init_ota(){
  ArduinoOTA.setHostname(OTA_NAME);
  ArduinoOTA.setPassword(OTA_PASS);
  ArduinoOTA.onStart([]() {
      Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
      Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });

  ArduinoOTA.begin();
  Serial.println("OTA ready");
  return 0;
}

int init_mdns(){
  if (MDNS.begin(MDNS_NAME)) {              // Start the mDNS responder for esp8266.local
    Serial.println("mDNS responder started");
    return 0;
  } else {
    Serial.println("Error setting up MDNS responder!");
    return -1;
  }
}

int init_web(){
  server.on("/", HTTP_GET, handleRoot);               // Call the 'handleRoot' function when a client requests URI "/"
  server.on("/Remote_Interface", HTTP_POST, handleRemote);
  server.on("/pump", HTTP_POST, handlePumpRequest);

  server.onNotFound([]() {                              // If the client requests any URI
    if (!handleFileRead(server.uri()))                  // send it if it exists
      server.send(404, "text/plain", "404: Not Found"); // otherwise, respond with a 404 (Not Found) error
  });

  server.begin();                           // Actually start the server
  Serial.println("HTTP server started");
  return 0;
}

// Display Functions:
void draw_function(Adafruit_SSD1306* oled){
  char header_text[50];
  generate_header_string(header_text); 
  oled->setCursor(1, 6);
  oled->print(header_text);
  oled->drawFastHLine(0, 14, 128, WHITE);
}


void setup() {
  // Set up pumps
  for(int i = 0; i < NUM_PUMPS; ++i){
    pumps[i].init(pins, enable_pins[i]);
  }

  // Set up display
  display.init_display(128, 64, 0x3C, -1);
  display.draw_splash();

  // Set up serial
  Serial.begin(115200);
  delay(10);
  Serial.println('\n');
  
  // Load config file
  if(!SPIFFS.begin()){
    Serial.println("Loading FS failed");
  }
  else{
    loadConfig();
  }
  
  // Connect to wifi
  init_wifi();

  // Set up OTA
  init_ota();

  // Set up MDNS
  init_mdns();

  // Set up webserver
  init_web();

  // Register main draw function:
  display.register_draw_func(draw_function);
  display.handle_display();
}


void loop() {
  // put your main code here, to run repeatedly:
  ArduinoOTA.handle();
  server.handleClient();

  //handle steppers
  bool is_slave = 0;
  for(int i = 0; i < NUM_PUMPS; ++i){
    if(pumps[i].get_steps_remaining() != 0){
      pumps[i].enable(is_slave);
      is_slave = 1;
    }
  }
  for(int i = 0; i < NUM_PUMPS; ++i){
    pumps[i].handle_stepper();
  }

  //handle display
  // display.handle_display();


  delayMicroseconds(1000);
}