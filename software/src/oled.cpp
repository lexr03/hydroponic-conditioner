#include "oled.hpp"

void Display::init_display(int w, int h, int addr, int rst_pin){
    needs_update = 0;

    screen_w = w;
    screen_h = h;
    screen_addr = addr;
    screen_rst_pin = rst_pin;

    oled_display = new Adafruit_SSD1306(screen_w, screen_h, &Wire, -1);
    if (!oled_display->begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x64
        Serial.println(F("SSD1306 allocation failed"));
    }

    oled_display->setTextColor(WHITE);
    oled_display->clearDisplay();
}

void Display::clear_display(){
    oled_display->clearDisplay();
}

void Display::update_display(){
    oled_display->display();
}

void Display::test_display(){
    Serial.println("OLED: test_display()");
    oled_display->print("TEST PRINT\n");
    update_display();
}

void Display::draw_splash(){
    oled_display->drawBitmap(0, 0, zero_three_spash, 128, 64, 1);
    update_display();
}

void Display::draw_header(const char* header_text){
    oled_display->setCursor(1, 0);
    oled_display->print(header_text);
    oled_display->drawFastHLine(0, 8, 128, WHITE);
    needs_update = 1;
}

void Display::register_draw_func(DrawFunctionCallbackType user_func){
    usr_draw_func = user_func;
}

void Display::draw_user(){
    usr_draw_func(oled_display);
}

void Display::handle_display(){
    clear_display();
    draw_user();
    update_display();
}