#ifndef STEPPER_H
#define STEPPER_H

#include <Arduino.h>

class Stepper_28BYJ{
public:
    Stepper_28BYJ(){};
    Stepper_28BYJ(uint8_t pins[4], uint8_t enable);
    void init(uint8_t pins[4], uint8_t enable);
    void set_target(int steps);
    void handle_stepper();
    int get_steps_remaining();

    void enable(bool set_slave);
    void step();
    void disable();
private:
    int steps_remaining;
    int steps_total;
    bool is_slave;

    uint8_t enable_pin;
    uint8_t motor_pins[4];
    uint8_t step_lut[8] = {0x1, 0x3, 0x2, 0x6, 0x4, 0xc, 0x8, 0x9};
    uint8_t state;
};

#endif