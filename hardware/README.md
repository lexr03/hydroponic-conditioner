# **hydroponic-conditioner v0.1 Hardware Guide**

## **Electrical Components**
The `electrical` directory contains KiCad files of the following components:

### **mcu-carrier**
The `mcu-carrier` holds the WeMos D1 mini, and the 3 `stepper-signal-switch` modules.

### **stepper-driver**
The `stepper-driver` board supplies the high-current 5v required to drive the two 28BYJ-48 stepper motors.

### **stepper-signal-switch**
The `stepper-signal-switch` module is used to mux the signal coming from the esp8266 across the 3 motors, which reduces the required pincount to drive the motors from 12 to 7.

Software logic is used to enable/disable steppers and allow them to share a common step signal.

---

## **Mechanical Components**
The `mechanical` directory contains `.stl` and `.step` files of the following componnents (excluding the `pump`)
### **controller-housing**
The controller housing encloses the mcu-carrier and the stepper-signal-switch daughterboards.

### **pump-bracket**
The pump bracket is used to affix the pump to 2020 aluminum extrusion, as well as hold the stepper-driver carrier, and the wire guide.

### **pump**
This design uses the peristaltic pump available [here](https://www.thingiverse.com/thing:254956) (distributed under a CC BY-SA 3.0 License), however any pump using 2 28BYJ-48 stepper motors will work.

---