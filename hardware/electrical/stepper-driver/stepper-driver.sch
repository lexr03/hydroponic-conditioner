EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2800 3150 3200 3150
Wire Wire Line
	3200 1800 3200 2150
Wire Wire Line
	2100 1800 3200 1800
Wire Wire Line
	2100 3250 2100 3100
Wire Wire Line
	2300 3650 2300 3900
Connection ~ 2300 3650
Wire Wire Line
	1450 3650 2300 3650
Wire Wire Line
	2250 3550 2250 4000
Connection ~ 2250 3550
Wire Wire Line
	1450 3550 2250 3550
Wire Wire Line
	2200 3450 2200 4100
Connection ~ 2200 3450
Wire Wire Line
	1450 3450 2200 3450
Wire Wire Line
	2150 3350 2150 4200
Connection ~ 2150 3350
Wire Wire Line
	1450 3350 2150 3350
Connection ~ 2350 3150
Wire Wire Line
	2300 2650 2300 3650
Wire Wire Line
	2250 2550 2250 3550
Wire Wire Line
	2200 2450 2200 3450
Wire Wire Line
	2150 2350 2150 3350
Wire Wire Line
	2350 2950 2350 3150
Wire Wire Line
	2350 4500 2350 4700
Connection ~ 2350 4500
Wire Wire Line
	2400 4500 2350 4500
Wire Wire Line
	2350 4400 2350 4500
Connection ~ 2350 4400
Wire Wire Line
	2350 4300 2350 4400
Wire Wire Line
	2400 4400 2350 4400
Wire Wire Line
	2350 4700 2800 4700
Wire Wire Line
	2400 4300 2350 4300
Connection ~ 2350 2850
Wire Wire Line
	2350 2750 2350 2850
Wire Wire Line
	2400 2750 2350 2750
Connection ~ 2350 2950
Wire Wire Line
	2350 2850 2350 2950
Wire Wire Line
	2400 2850 2350 2850
Wire Wire Line
	2400 2950 2350 2950
Connection ~ 2800 3150
Wire Wire Line
	2350 3150 2800 3150
$Comp
L Connector:Conn_01x06_Male J3
U 1 1 600FB691
P 1250 3350
F 0 "J3" H 1358 3731 50  0000 C CNN
F 1 "Conn_01x06_Male" H 1358 3640 50  0000 C CNN
F 2 "Connector_JST:JST_XH_B6B-XH-A_1x06_P2.50mm_Vertical" H 1250 3350 50  0001 C CNN
F 3 "~" H 1250 3350 50  0001 C CNN
	1    1250 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 2350 3400 2350
Wire Wire Line
	3200 2450 3400 2450
Wire Wire Line
	3200 2550 3400 2550
Wire Wire Line
	3200 2650 3400 2650
Wire Wire Line
	3200 4200 3400 4200
Wire Wire Line
	3200 4100 3400 4100
Wire Wire Line
	3200 4000 3400 4000
Wire Wire Line
	3200 3900 3400 3900
$Comp
L Connector:Conn_01x05_Male J2
U 1 1 600E7FE2
P 3600 4100
F 0 "J2" H 3572 4032 50  0000 R CNN
F 1 "Conn_01x05_Male" H 3572 4123 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B5B-XH-A_1x05_P2.50mm_Vertical" H 3600 4100 50  0001 C CNN
F 3 "~" H 3600 4100 50  0001 C CNN
	1    3600 4100
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x05_Male J1
U 1 1 600E5DDF
P 3600 2550
F 0 "J1" H 3572 2482 50  0000 R CNN
F 1 "Conn_01x05_Male" H 3572 2573 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B5B-XH-A_1x05_P2.50mm_Vertical" H 3600 2550 50  0001 C CNN
F 3 "~" H 3600 2550 50  0001 C CNN
	1    3600 2550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2300 2650 2400 2650
Wire Wire Line
	2400 3900 2300 3900
Wire Wire Line
	2250 4000 2400 4000
Wire Wire Line
	2400 2550 2250 2550
Wire Wire Line
	2200 2450 2400 2450
Wire Wire Line
	2400 4100 2200 4100
Wire Wire Line
	2150 4200 2400 4200
Wire Wire Line
	2400 2350 2150 2350
$Comp
L Transistor_Array:ULN2003A U2
U 1 1 600E21AD
P 2800 4100
F 0 "U2" H 2800 4767 50  0000 C CNN
F 1 "ULN2003A" H 2800 4676 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 2850 3550 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/uln2003a.pdf" H 2900 3900 50  0001 C CNN
	1    2800 4100
	1    0    0    -1  
$EndComp
$Comp
L Transistor_Array:ULN2003A U1
U 1 1 600DF841
P 2800 2550
F 0 "U1" H 2800 3217 50  0000 C CNN
F 1 "ULN2003A" H 2800 3126 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 2850 2000 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/uln2003a.pdf" H 2900 2350 50  0001 C CNN
	1    2800 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 4700 2050 4700
Wire Wire Line
	2050 4700 2050 3250
Connection ~ 2350 4700
Wire Wire Line
	2050 3150 2350 3150
$Comp
L Device:C C1
U 1 1 60142819
P 3200 3300
F 0 "C1" H 3315 3346 50  0000 L CNN
F 1 "C" H 3315 3255 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 3238 3150 50  0001 C CNN
F 3 "~" H 3200 3300 50  0001 C CNN
	1    3200 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 3250 3050 3250
Wire Wire Line
	3050 3250 3050 3450
Wire Wire Line
	3050 3450 3200 3450
Wire Wire Line
	3200 3700 3200 3450
Connection ~ 3200 3450
Wire Wire Line
	1450 3150 1800 3150
Wire Wire Line
	1800 3150 1800 3100
Wire Wire Line
	1800 3100 2100 3100
Connection ~ 2100 3100
Wire Wire Line
	2100 3100 2100 1800
Wire Wire Line
	1450 3250 2050 3250
Connection ~ 2050 3250
Wire Wire Line
	2050 3250 2050 3150
Wire Wire Line
	3200 2150 3300 2150
Wire Wire Line
	3300 2150 3300 2750
Wire Wire Line
	3300 2750 3400 2750
Connection ~ 3200 2150
Wire Wire Line
	3200 3700 3300 3700
Wire Wire Line
	3300 3700 3300 4300
Wire Wire Line
	3300 4300 3400 4300
Connection ~ 3200 3700
$EndSCHEMATC
