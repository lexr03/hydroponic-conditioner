EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L misc:D1_Mini U?
U 1 1 601ECA8B
P 1650 2850
AR Path="/600DF123/601ECA8B" Ref="U?"  Part="1" 
AR Path="/601ECA8B" Ref="U1"  Part="1" 
F 0 "U1" H 1675 3591 79  0000 C CNN
F 1 "D1_Mini" H 1675 3456 79  0000 C CNN
F 2 "misc:D1_Mini" H 1650 3400 79  0001 C CNN
F 3 "" H 1650 3400 79  0001 C CNN
	1    1650 2850
	1    0    0    -1  
$EndComp
NoConn ~ 1350 2400
NoConn ~ 1350 2500
NoConn ~ 1350 3200
NoConn ~ 2000 3400
NoConn ~ 2000 3300
Text Label 2450 2400 2    50   ~ 0
ENBL_0
Wire Wire Line
	2450 2400 2000 2400
Text Label 2450 2900 2    50   ~ 0
SIG_C
Wire Wire Line
	2450 2900 2000 2900
Text Label 2450 3000 2    50   ~ 0
SIG_D
Wire Wire Line
	2450 3000 2000 3000
Text Label 2450 3100 2    50   ~ 0
ENBL_2
Wire Wire Line
	2450 3100 2000 3100
Text Label 2450 2700 2    50   ~ 0
SIG_A
Wire Wire Line
	2450 2700 2000 2700
Text Label 2450 2800 2    50   ~ 0
SIG_B
Wire Wire Line
	2450 2800 2000 2800
Text Label 2450 3200 2    50   ~ 0
ENBL_1
Wire Wire Line
	2450 3200 2000 3200
Wire Wire Line
	2450 2500 2000 2500
Text Label 2450 2600 2    50   ~ 0
D1_SDA
Wire Wire Line
	2450 2600 2000 2600
Text Label 2450 2500 2    50   ~ 0
D1_SCL
Text Label 900  3300 0    50   ~ 0
PWR
Wire Wire Line
	900  3300 1350 3300
Text Label 900  3400 0    50   ~ 0
GND
Wire Wire Line
	900  3400 1350 3400
Wire Notes Line
	750  1850 750  3550
Wire Notes Line
	750  3550 2550 3550
Wire Notes Line
	2550 3550 2550 1850
Wire Notes Line
	2550 1850 750  1850
Text Notes 800  1950 0    50   ~ 0
MCU
$Comp
L Connector:Barrel_Jack_Switch J?
U 1 1 601ECAB1
P 1100 1400
AR Path="/600DF123/601ECAB1" Ref="J?"  Part="1" 
AR Path="/601ECAB1" Ref="J2"  Part="1" 
F 0 "J2" H 1157 1717 50  0000 C CNN
F 1 "Barrel_Jack_Switch" H 1157 1626 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_Horizontal" H 1150 1360 50  0001 C CNN
F 3 "~" H 1150 1360 50  0001 C CNN
	1    1100 1400
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C?
U 1 1 601ECAB7
P 1800 1450
AR Path="/600DF123/601ECAB7" Ref="C?"  Part="1" 
AR Path="/601ECAB7" Ref="C1"  Part="1" 
F 0 "C1" H 1918 1496 50  0000 L CNN
F 1 "CP" H 1918 1405 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P5.00mm" H 1838 1300 50  0001 C CNN
F 3 "~" H 1800 1450 50  0001 C CNN
	1    1800 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 1300 1800 1300
Connection ~ 1800 1300
Wire Wire Line
	1800 1300 2450 1300
Text Label 2450 1300 2    50   ~ 0
PWR
Wire Wire Line
	1400 1500 1500 1500
Wire Wire Line
	1500 1500 1500 1600
Wire Wire Line
	1500 1600 1800 1600
Wire Wire Line
	1800 1600 2450 1600
Connection ~ 1800 1600
Text Label 2450 1600 2    50   ~ 0
GND
Wire Notes Line
	700  750  700  1700
Wire Notes Line
	700  1700 2600 1700
Wire Notes Line
	2600 1700 2600 750 
Wire Notes Line
	2600 750  700  750 
Text Notes 750  850  0    50   ~ 0
POWER
Text Label 3200 1600 0    50   ~ 0
SIG_A
Text Label 3200 1700 0    50   ~ 0
SIG_B
Text Label 3200 1900 0    50   ~ 0
SIG_C
Text Label 3200 2000 0    50   ~ 0
SIG_D
Wire Wire Line
	3200 2000 3700 2000
Wire Wire Line
	3200 1900 3700 1900
Wire Wire Line
	3200 1700 3700 1700
Wire Wire Line
	3200 1600 3700 1600
Text Label 3200 1500 0    50   ~ 0
GND
Wire Wire Line
	3200 1500 3700 1500
Text Label 3200 2100 0    50   ~ 0
PWR
Wire Wire Line
	3200 2100 3700 2100
Text Label 3200 1800 0    50   ~ 0
ENBL_0
Wire Wire Line
	3200 1800 3700 1800
Wire Notes Line
	2900 850  2900 2800
Wire Notes Line
	2900 2800 4300 2800
Wire Notes Line
	4300 2800 4300 850 
Wire Notes Line
	4300 850  2900 850 
$Comp
L Connector:Conn_01x07_Female J1
U 1 1 60217B6B
P 3900 1800
F 0 "J1" H 3500 1400 50  0000 L CNN
F 1 "Conn_01x07_Female" H 3200 1300 50  0000 L CNN
F 2 "misc:enable-switch-connector" H 3900 1800 50  0001 C CNN
F 3 "~" H 3900 1800 50  0001 C CNN
	1    3900 1800
	1    0    0    -1  
$EndComp
Text Label 3200 3700 0    50   ~ 0
SIG_A
Text Label 3200 3800 0    50   ~ 0
SIG_B
Text Label 3200 4000 0    50   ~ 0
SIG_C
Text Label 3200 4100 0    50   ~ 0
SIG_D
Wire Wire Line
	3200 4100 3700 4100
Wire Wire Line
	3200 4000 3700 4000
Wire Wire Line
	3200 3800 3700 3800
Wire Wire Line
	3200 3700 3700 3700
Text Label 3200 3600 0    50   ~ 0
GND
Wire Wire Line
	3200 3600 3700 3600
Text Label 3200 4200 0    50   ~ 0
PWR
Wire Wire Line
	3200 4200 3700 4200
Text Label 3200 3900 0    50   ~ 0
ENBL_1
Wire Wire Line
	3200 3900 3700 3900
Wire Notes Line
	2900 2950 2900 4900
Wire Notes Line
	2900 4900 4300 4900
Wire Notes Line
	4300 4900 4300 2950
Wire Notes Line
	4300 2950 2900 2950
$Comp
L Connector:Conn_01x07_Female J3
U 1 1 60238E6C
P 3900 3900
F 0 "J3" H 3500 3500 50  0000 L CNN
F 1 "Conn_01x07_Female" H 3200 3400 50  0000 L CNN
F 2 "misc:enable-switch-connector" H 3900 3900 50  0001 C CNN
F 3 "~" H 3900 3900 50  0001 C CNN
	1    3900 3900
	1    0    0    -1  
$EndComp
Text Label 3200 5800 0    50   ~ 0
SIG_A
Text Label 3200 5900 0    50   ~ 0
SIG_B
Text Label 3200 6100 0    50   ~ 0
SIG_C
Text Label 3200 6200 0    50   ~ 0
SIG_D
Wire Wire Line
	3200 6200 3700 6200
Wire Wire Line
	3200 6100 3700 6100
Wire Wire Line
	3200 5900 3700 5900
Wire Wire Line
	3200 5800 3700 5800
Text Label 3200 5700 0    50   ~ 0
GND
Wire Wire Line
	3200 5700 3700 5700
Text Label 3200 6300 0    50   ~ 0
PWR
Wire Wire Line
	3200 6300 3700 6300
Text Label 3200 6000 0    50   ~ 0
ENBL_2
Wire Wire Line
	3200 6000 3700 6000
Wire Notes Line
	2900 5050 2900 7000
Wire Notes Line
	2900 7000 4300 7000
Wire Notes Line
	4300 7000 4300 5050
Wire Notes Line
	4300 5050 2900 5050
$Comp
L Connector:Conn_01x07_Female J4
U 1 1 6023AB79
P 3900 6000
F 0 "J4" H 3500 5600 50  0000 L CNN
F 1 "Conn_01x07_Female" H 3200 5500 50  0000 L CNN
F 2 "misc:enable-switch-connector" H 3900 6000 50  0001 C CNN
F 3 "~" H 3900 6000 50  0001 C CNN
	1    3900 6000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Male J5
U 1 1 602581B8
P 5400 1650
F 0 "J5" H 5600 1350 50  0000 R CNN
F 1 "Conn_01x04_Male" H 5900 1200 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B4B-XH-A_1x04_P2.50mm_Vertical" H 5400 1650 50  0001 C CNN
F 3 "~" H 5400 1650 50  0001 C CNN
	1    5400 1650
	-1   0    0    -1  
$EndComp
Text Label 4700 1650 0    50   ~ 0
D1_SCL
Text Label 4700 1750 0    50   ~ 0
GND
Wire Wire Line
	4700 1750 5200 1750
Wire Wire Line
	4700 1650 5200 1650
Text Label 4700 1550 0    50   ~ 0
D1_SDA
Wire Wire Line
	4700 1550 5200 1550
Text Label 4700 1850 0    50   ~ 0
PWR
Wire Wire Line
	4700 1850 5200 1850
Wire Notes Line
	4600 2250 5700 2250
Wire Notes Line
	5700 2250 5700 1200
Wire Notes Line
	5700 1200 4600 1200
Wire Notes Line
	4600 1200 4600 2250
Text Notes 4650 1350 0    50   ~ 0
Display Connector\n
$EndSCHEMATC
