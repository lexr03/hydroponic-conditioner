EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L misc:CD4066B U?
U 1 1 60190B78
P 4950 1800
AR Path="/600DF123/60190B78" Ref="U?"  Part="1" 
AR Path="/60190B78" Ref="U1"  Part="1" 
F 0 "U1" H 4975 2565 50  0000 C CNN
F 1 "CD4066B" H 4975 2474 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_LongPads" H 4950 2500 50  0001 C CNN
F 3 "" H 4950 2500 50  0001 C CNN
	1    4950 1800
	1    0    0    -1  
$EndComp
Text Label 3800 1300 0    50   ~ 0
SIG_A
Text Label 3800 1400 0    50   ~ 0
SIG_B
Text Label 3800 1500 0    50   ~ 0
SIG_C
Text Label 3800 1900 0    50   ~ 0
ENBL
Wire Wire Line
	3800 1900 4200 1900
Wire Wire Line
	3800 1500 4300 1500
Wire Wire Line
	3800 1400 4300 1400
Wire Wire Line
	3800 1300 4300 1300
Wire Wire Line
	4300 2000 4200 2000
Wire Wire Line
	4200 2000 4200 1900
Connection ~ 4200 1900
Wire Wire Line
	4200 1900 4300 1900
Wire Wire Line
	4300 2100 4200 2100
Wire Wire Line
	4200 2100 4200 2000
Connection ~ 4200 2000
Wire Wire Line
	4300 2200 4200 2200
Wire Wire Line
	4200 2200 4200 2100
Connection ~ 4200 2100
Text Label 3800 2450 0    50   ~ 0
GND
Wire Wire Line
	3800 2450 4300 2450
Text Label 3800 2550 0    50   ~ 0
PWR
Wire Wire Line
	3800 2550 4300 2550
$Comp
L Connector:Conn_01x06_Male J?
U 1 1 60190B96
P 6300 1300
AR Path="/600DF123/60190B96" Ref="J?"  Part="1" 
AR Path="/60190B96" Ref="J2"  Part="1" 
F 0 "J2" H 6450 900 50  0000 R CNN
F 1 "Conn_01x06_Male" H 6850 800 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B6B-XH-A_1x06_P2.50mm_Vertical" H 6300 1300 50  0001 C CNN
F 3 "~" H 6300 1300 50  0001 C CNN
	1    6300 1300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5650 1600 6100 1600
Wire Wire Line
	5650 1500 6100 1500
Wire Wire Line
	5650 1400 6100 1400
Wire Wire Line
	5650 1300 6100 1300
Wire Wire Line
	6100 1100 5650 1100
Text Label 5650 1100 0    50   ~ 0
PWR
Text Label 5650 1200 0    50   ~ 0
GND
Wire Wire Line
	5650 1200 6100 1200
Wire Notes Line
	2700 650  2700 2800
Wire Notes Line
	6550 2800 6550 650 
Text Notes 2800 800  0    50   ~ 0
STEPPER ENABLE
$Comp
L Device:LED D?
U 1 1 60190BA9
P 6250 2450
AR Path="/600DF123/60190BA9" Ref="D?"  Part="1" 
AR Path="/60190BA9" Ref="D1"  Part="1" 
F 0 "D1" V 6289 2332 50  0000 R CNN
F 1 "LED" V 6198 2332 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 6250 2450 50  0001 C CNN
F 3 "~" H 6250 2450 50  0001 C CNN
	1    6250 2450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 60190BAF
P 6100 2300
AR Path="/600DF123/60190BAF" Ref="R?"  Part="1" 
AR Path="/60190BAF" Ref="R1"  Part="1" 
F 0 "R1" V 5893 2300 50  0000 C CNN
F 1 "1K8R" V 5984 2300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6030 2300 50  0001 C CNN
F 3 "~" H 6100 2300 50  0001 C CNN
	1    6100 2300
	0    1    1    0   
$EndComp
Text Label 5650 2300 0    50   ~ 0
ENBL
Wire Wire Line
	5650 2300 5950 2300
Text Label 5650 2600 0    50   ~ 0
GND
Wire Wire Line
	5650 2600 6250 2600
$Comp
L Connector:Conn_01x05_Male J?
U 1 1 601988F1
P 2850 1850
AR Path="/600DF123/601988F1" Ref="J?"  Part="1" 
AR Path="/601988F1" Ref="J1"  Part="1" 
F 0 "J1" H 3000 1450 50  0000 R CNN
F 1 "Conn_01x05_Male" H 3400 1350 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 2850 1850 50  0001 C CNN
F 3 "~" H 2850 1850 50  0001 C CNN
	1    2850 1850
	1    0    0    -1  
$EndComp
Text Label 3550 1650 2    50   ~ 0
SIG_A
Text Label 3550 1750 2    50   ~ 0
SIG_B
Text Label 3550 1950 2    50   ~ 0
SIG_C
Text Label 3550 2050 2    50   ~ 0
SIG_D
Wire Wire Line
	3550 2050 3050 2050
Wire Wire Line
	3550 1950 3050 1950
Wire Wire Line
	3550 1750 3050 1750
Wire Wire Line
	3550 1650 3050 1650
Text Label 3550 1050 2    50   ~ 0
GND
Wire Wire Line
	3550 1050 3050 1050
Text Label 3550 1350 2    50   ~ 0
PWR
Wire Wire Line
	3550 1350 3050 1350
Text Label 3800 1600 0    50   ~ 0
SIG_D
Wire Wire Line
	3800 1600 4300 1600
Text Label 3550 1850 2    50   ~ 0
ENBL
Wire Wire Line
	3550 1850 3050 1850
Wire Notes Line
	2700 650  6550 650 
Wire Notes Line
	2700 2800 6550 2800
$Comp
L Connector:Conn_01x01_Male J3
U 1 1 601C1048
P 2850 1050
F 0 "J3" H 2958 1231 50  0000 C CNN
F 1 "Conn_01x01_Male" H 2958 1140 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 2850 1050 50  0001 C CNN
F 3 "~" H 2850 1050 50  0001 C CNN
	1    2850 1050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J4
U 1 1 601C1650
P 2850 1350
F 0 "J4" H 2958 1531 50  0000 C CNN
F 1 "Conn_01x01_Male" H 2958 1440 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 2850 1350 50  0001 C CNN
F 3 "~" H 2850 1350 50  0001 C CNN
	1    2850 1350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
