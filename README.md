# **hydroponic-conditioner v0.1**

## **Purpose**
This project is an IoT hydroponic water conditioner control system capable of conditioning water using up to 3 chemicals.


![Hydroponic Conditioner v0.1](images/hydroponic-conditionerv0.1.jpg)

---
## **Control**
The conditioner is capable of controlling up to 3 pumps (numbered 0-2 from top to bottom) via either of the following ways:

### **Web Interface**
The web interface available by navigating to the pump controller's IP address in your browser.

### **JSON Payload**
A JSON payload may be sent to the controller's `/Remote_Interface` page with the following format:

`{<PUMP #>:{<ACTION>}}`

Multiple pumps can be controlled at once using the following format:

`{<PUMP #>:{<ACTION>}, <PUMP #>:{<ACTION>}}`

Valid actions are given in the following table:

| Action | Example |
| ---- | ---------- |
| "ml":N | pump N ml


**Example:** For a conditioner located at `192.168.0.2`, to pump 10ml from pump 0, and 30ml from pump 2 the JSON payload sent to `192.168.0.2/Remote_Interface` would be as follows:

`{"0":{"ml":10}, "2":{"ml":30}}`

---

## **v0.1 Features**
- Basic Web Interface to manually set pump 
- Ability to read configuration information from SPI FS file config.json
- Control up to 3 pumps
---